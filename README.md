# Cookie First

Drupal integration for the cookie management solution [Cookie First](https://cookiefirst.com/).

## Installation

1. Download and enable [Cookie First module](https://www.drupal.org/project/cookiefirst).

2. Configure at Configuration > Web services > Cookie first (requires the administer site configuration permission).

## Features

- Enable / Disable the module
- Add filters for the scripts on the websites
- Add filters for the iframes on the websites
- Configure the message when a iFrame is disabled because its category has not been enabled

