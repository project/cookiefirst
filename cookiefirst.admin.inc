<?php

function cookiefirst_settings_form($form, &$form_state) {
  $form['cookiefirst_active'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable Cookie First'),
    '#default_value' => variable_get('cookiefirst_active'),
  ];

  $form['cookiefirst_api_key'] = [
    '#type' => 'textfield',
    '#title' => t('Cookiefirst API key'),
    '#default_value' => variable_get('cookiefirst_api_key'),
    '#required' => TRUE,
  ];

  $form['filters_title'] = [
    '#type' => 'item',
    '#title' => t('Filters & messages per categories'),
  ];

  $form['filters'] = [
    '#type' => 'vertical_tabs',
  ];

  $form['filters']['performance'] = [
    '#type' => 'fieldset',
    '#title' => t('Performance'),
  ];
  $form['filters']['performance']['cookiefirst_filter_performance_scripts'] = [
    '#type' => 'textarea',
    '#title' => t('Scripts'),
    '#description' => t('One per line, the scripts are filtered by regex.'),
    '#default_value' => variable_get('cookiefirst_filter_performance_scripts'),
  ];
  $form['filters']['performance']['iframe'] = [
    '#type' => 'fieldset',
    '#title' => t('iFrame'),
  ];
  $form['filters']['performance']['iframe']['cookiefirst_filter_performance_iframes'] = [
    '#type' => 'textarea',
    '#title' => t('iFrames domains'),
    '#description' => t('One per line, the iframes are filtered by regex.'),
    '#default_value' => variable_get('cookiefirst_filter_performance_iframes'),
  ];
  $form['filters']['performance']['iframe']['cookiefirst_performance_iframe_message'] = [
    '#type' => 'textarea',
    '#title' => t('Blocked iFrame message'),
    '#description' => t('This message will be shown when the user has not accepted the category "performance".'),
    '#default_value' => variable_get('cookiefirst_performance_iframe_message') ? variable_get('cookiefirst_performance_iframe_message') : "This content cannot be displayed because you did not accept the 'performance' cookies.\nTo view this content, please enable 'performance' cookies by clicking on the button bellow.",
    '#required' => TRUE,
  ];
  $form['filters']['performance']['iframe']['cookiefirst_performance_iframe_button'] = [
    '#type' => 'textfield',
    '#title' => t('Unlock iFrame button'),
    '#description' => t('This button will accept all cookies in the category "performance" when clicked and will display the associated iFrames.'),
    '#default_value' => variable_get('cookiefirst_performance_iframe_button') ? variable_get('cookiefirst_performance_iframe_button') : "Enable 'performance' cookies",
    '#required' => TRUE,
  ];

  $form['filters']['functional'] = [
    '#type' => 'fieldset',
    '#title' => t('Functional'),
  ];
  $form['filters']['functional']['cookiefirst_filter_functional_scripts'] = [
    '#type' => 'textarea',
    '#title' => t('Scripts'),
    '#description' => t('One per line, the scripts are filtered by regex.'),
    '#default_value' => variable_get('cookiefirst_filter_functional_scripts'),
  ];
  $form['filters']['functional']['iframe'] = [
    '#type' => 'fieldset',
    '#title' => t('iFrame'),
  ];
  $form['filters']['functional']['iframe']['cookiefirst_filter_functional_iframes'] = [
    '#type' => 'textarea',
    '#title' => t('iFrames domains'),
    '#description' => t('One per line, the iframes are filtered by regex.'),
    '#default_value' => variable_get('cookiefirst_filter_functional_iframes'),
  ];
  $form['filters']['functional']['iframe']['cookiefirst_functional_iframe_message'] = [
    '#type' => 'textarea',
    '#title' => t('Blocked iFrame message'),
    '#description' => t('This message will be shown when the user has not accepted the category "functional".'),
    '#default_value' => variable_get('cookiefirst_functional_iframe_message') ? variable_get('cookiefirst_functional_iframe_message') : "This content cannot be displayed because you did not accept the 'functional' cookies.\nTo view this content, please enable 'functional' cookies by clicking on the button bellow.",
    '#required' => TRUE,
  ];
  $form['filters']['functional']['iframe']['cookiefirst_functional_iframe_button'] = [
    '#type' => 'textfield',
    '#title' => t('Unlock iFrame button'),
    '#description' => t('This button will accept all cookies in the category "functional" when clicked and will display the associated iFrames.'),
    '#default_value' => variable_get('cookiefirst_functional_iframe_button') ? variable_get('cookiefirst_functional_iframe_button') : "Enable 'functional' cookies",
    '#required' => TRUE,
  ];

  $form['filters']['advertising'] = [
    '#type' => 'fieldset',
    '#title' => t('Advertising'),
  ];
  $form['filters']['advertising']['cookiefirst_filter_advertising_scripts'] = [
    '#type' => 'textarea',
    '#title' => t('Scripts'),
    '#description' => t('One per line, the scripts are filtered by regex.'),
    '#default_value' => variable_get('cookiefirst_filter_advertising_scripts'),
  ];
  $form['filters']['advertising']['iframe'] = [
    '#type' => 'fieldset',
    '#title' => t('iFrame'),
  ];
  $form['filters']['advertising']['iframe']['cookiefirst_filter_advertising_iframes'] = [
    '#type' => 'textarea',
    '#title' => t('iFrames domains'),
    '#description' => t('One per line, the iframes are filtered by regex.'),
    '#default_value' => variable_get('cookiefirst_filter_advertising_iframes'),
  ];
  $form['filters']['advertising']['iframe']['cookiefirst_advertising_iframe_message'] = [
    '#type' => 'textarea',
    '#title' => t('Blocked iFrame message'),
    '#description' => t('This message will be shown when the user has not accepted the category "advertising".'),
    '#default_value' => variable_get('cookiefirst_advertising_iframe_message') ? variable_get('cookiefirst_advertising_iframe_message') : "This content cannot be displayed because you did not accept the 'advertising' cookies.\nTo view this content, please enable 'advertising' cookies by clicking on the button bellow.",
    '#required' => TRUE,
  ];
  $form['filters']['advertising']['iframe']['cookiefirst_advertising_iframe_button'] = [
    '#type' => 'textfield',
    '#title' => t('Unlock iFrame button'),
    '#description' => t('This button will accept all cookies in the category "advertising" when clicked and will display the associated iFrames.'),
    '#default_value' => variable_get('cookiefirst_advertising_iframe_button') ? variable_get('cookiefirst_advertising_iframe_button') : "Enable 'advertising' cookies",
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}
