<?php

/**
 * Implements hook_menu().
 */
function cookiefirst_menu() {
  $items['admin/config/services/cookiefirst'] = [
    'title' => 'Cookie First',
    'position' => 'right',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cookiefirst_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'cookiefirst.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  ];

  return $items;
}

/**
 * Override or insert variables into the html template.
 */
function cookiefirst_preprocess_html(&$vars) {
  $active = variable_get('cookiefirst_active') && !path_is_admin(current_path());
  $apiKey = variable_get('cookiefirst_api_key');

  if ($active && empty($apiKey)) {
    drupal_set_message('Cookie first api key is not configured, please configure it to enable the module', 'warning');
  } else if($active) {
    drupal_add_js('https://consent.cookiefirst.com/banner.js?cookiefirst-key=' . variable_get('cookiefirst_api_key'), [
      'type' => 'external',
      'weight' => 0
    ]);
  }

  drupal_add_js(['cookiefirst' => [
    'enabled' => $active,
    'categories' => [
      'performance' => [
        'domains' => explode(PHP_EOL, variable_get('cookiefirst_filter_performance_iframes')),
        'message' => variable_get('cookiefirst_performance_iframe_message'),
        'button' => variable_get('cookiefirst_performance_iframe_button'),
      ],
      'functional' => [
        'domains' => explode(PHP_EOL, variable_get('cookiefirst_filter_functional_iframes')),
        'message' => variable_get('cookiefirst_functional_iframe_message'),
        'button' => variable_get('cookiefirst_functional_iframe_button'),
      ],
      'advertising' => [
        'domains' => explode(PHP_EOL, variable_get('cookiefirst_filter_advertising_iframes')),
        'message' => variable_get('cookiefirst_advertising_iframe_message'),
        'button' => variable_get('cookiefirst_advertising_iframe_button'),
      ],
    ],
  ]], ['type' => 'setting']);
  drupal_add_js(drupal_get_path('module', 'cookiefirst') . '/js/cookiefirst-alterIframes.js');
  drupal_add_css(drupal_get_path('module', 'cookiefirst') . '/css/cookiefirst.css');

}

function cookiefirst_preprocess_html_tag(&$vars) {
  $active = variable_get('cookiefirst_active');

  if ($vars['element']['#tag'] === 'script' && $active) {
    if (!empty($vars['element']['#value'])) {
      __cookiefirst_match_gdpr_script($vars['element']['#value'], $vars['element']['#attributes']);
    } else if (!empty($vars['element']['#attributes']['src']) && $active) {
      __cookiefirst_match_gdpr_script($vars['element']['#attributes']['src'], $vars['element']['#attributes']);
    }
  }
}

function cookiefirst_preprocess_field(&$variables, $hook) {
  $element = $variables['element'];

  if (in_array($element['#field_type'], [
    'text',
    'text_long',
    'text_with_summary',
  ])) {
    foreach ($variables['items'] as &$item) {
      if (isset($item['#markup']) && strpos($item['#markup'], '<iframe') !== FALSE) {
        $item['#markup'] = preg_replace('/(<iframe[^>]*)src(=(?:"|\')[^"|\']+(?:"|\')[^>]*>)/', '$1data-src$2', $item['#markup']);
      }
    }
  }
}

function __cookiefirst_match_gdpr_script($content, &$scriptAttributes) {
  __cookiefirst_alter_script($scriptAttributes, $content, 'performance', explode(PHP_EOL, variable_get('cookiefirst_filter_performance_scripts')));
  __cookiefirst_alter_script($scriptAttributes, $content, 'functional', explode(PHP_EOL, variable_get('cookiefirst_filter_functional_scripts')));
  __cookiefirst_alter_script($scriptAttributes, $content, 'advertising', explode(PHP_EOL, variable_get('cookiefirst_filter_advertising_scripts')));
}

function __cookiefirst_alter_script(&$scriptAttributes, $content, $category, $scripts) {
  $content = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", trim($content))); // Remove new lines and excess spaces

  foreach($scripts as $script) {
    $script = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", trim($script))); // Remove new lines and excess spaces

    if (strpos($content, $script) !== FALSE) {
      $scriptAttributes['data-cookiefirst-category'] = $category;
      $scriptAttributes['type'] = 'text/plain';
    }
  }
}
