(($, Drupal) => {
  Drupal.behaviors.cookiefirst = {
    attach: (context, settings) => {
      if (Drupal.settings.cookiefirst.enabled) {
        document.querySelectorAll('iframe[src]').forEach(iframe => {
          iframe.setAttribute('data-src', iframe.getAttribute('src'));
          iframe.removeAttribute('src');
        });

        window.addEventListener("cf_init", Drupal.behaviors.cookiefirst.updateIframes);
        window.addEventListener("cf_consent", Drupal.behaviors.cookiefirst.updateIframes);
      }
    },

    updateIframes: event => {
      let consent = event.detail || CookieFirst.consent;
      consent = consent === null ? {
        necessary: true, // necessary category can't be disabled.
        performance: false,
        functional: false,
        advertising: false,
      } : consent;

      Object.entries(Drupal.settings.cookiefirst.categories).forEach(category => {
        const [type, settings] = category;
        settings.domains.forEach(domain => {
          Drupal.behaviors.cookiefirst.verifyDomain(domain.trim(), type, consent, settings.message, settings.button);
        });
      });

      document.querySelectorAll('iframe:not([cookiefirst-category])').forEach(iframe => {
        iframe.setAttribute('src', iframe.getAttribute('data-src'));
        iframe.removeAttribute('data-src');
      });
    },

    verifyDomain: (domain, type, consent, message, buttonText) => {
      document.querySelectorAll('iframe').forEach(iframe => {
        if(domain !== "" && iframe.hasAttribute('data-src') && iframe.getAttribute('data-src').includes(domain) && consent[type] === false) {
          iframe.setAttribute('cookiefirst-category', type);
          iframe.style.display = 'none';

          const buttonDiv = document.createElement('div');
          buttonDiv.classList.add('dcf__disabled-resource');
          buttonDiv.innerText = message;

          const button = document.createElement('button');
          button.setAttribute('onclick', `CookieFirst.acceptCategory('${type}')`);
          button.innerText = buttonText;

          buttonDiv.appendChild(button);
          iframe.insertAdjacentElement('afterend', buttonDiv);
        }
      });
    },
  };

})(jQuery, Drupal);
